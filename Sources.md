# Modelling Reference Sources
## Hohes Schloss Füssen
### Corner Building
- [CornerBuilding](https://img3.oastatic.com/img2/27581144/max/hohes-schloss-fuessen.jpg) - Used for coat of arms
- [CornerBuilding1](http://www.burgenwelt.org/deutschland/fuessen/bild18.jpg)
- [CornerBuilding2](https://media.tourispo.com/images/ecu/entity/e_sight/ausflugsziel_hohes-schloss-fuessen_n70331-121337-1_l.jpg)
- [CornerBuilding3](https://burgenarchiv.de/grafiken/bilder/bay/hohes-schloss-fuessen12.jpg)

### Wall paintings
- [Paintings](https://www.fuessen.de/uploads/pics/hohes-schloss-fassade-detail.jpg) - Used for middle sized window decoration and bay window
- [Paintings1](https://live.staticflickr.com/4129/5016011824_5ae3fb3ea8_b.jpg)
- [Paintings2](https://media.tourispo.com/images/ecu/entity/e_sight/ausflugsziel_hohes-schloss-fuessen_n70331-121333-2_l.jpg)
- [Paintings3](https://3.bp.blogspot.com/-OUbssAyoMd4/WsIHMj0bXvI/AAAAAAAAJKg/MYV0Istg9T8VAOqVJfCgVfotkAVn0DZMQCEwYBhgL/s1600/Enlight1211.JPG) - Used for other variant of large painted bay window, turreted corner paintings
- [Paintings4](https://media-cdn.tripadvisor.com/media/photo-s/05/ee/15/4e/hohes-schloss.jpg)

### Tower
- [Tower](https://media-cdn.holidaycheck.com/w_1024,h_768,c_fit,q_auto,f_auto/ugc/images/a0cb9298-ab27-31cc-816f-ea0d64840bcf) - Used for Tower and Clock
- [Tower1](https://www.urlaubsziele.com/bild/sehenswuerdigkeiten/2162/hohes-schloss-100.jpg)

### Castle Side
- [Side](https://www.lechweg.com/fileadmin/user_upload/Hohes_Schloss_in_Fuessen__Andere_.jpg)
- [Side1](https://rund-uma-dum.de/wp-content/uploads/2018/06/Hohes-Schloss-02.jpg)
- [Side2](https://www.fuessen.de/uploads/pics/fuessens-romantische-altstadt-in-bayern_12.jpg)
- [Side3](https://n5c6c8q3.rocketcdn.me/wp-content/uploads/2018/05/Hohes-Schloss-Fuessen_0085_Panorama-678x381.jpg)
- [Side4](https://allgaeu-erleben.com/img/fuessen-stadtschloss.jpg)
- [Side5](https://www.allgaeubild.de/fileadmin/datenbank/ferien-im-fuessener-land/artikelbilder/mantel/Burgen%2BSchloesser_Hohes-Schloss_01_Pano.jpg)
- [Side6](https://kinder-im-allgaeu.de/data/media/40/Ausflugsziele_Hohes-Schloss-Fuessen-min.jpg-25368.jpg)
- [Side7](https://www.furnishedinside.com/blog-images/title/hohe-schloss-titel.jpeg)
- [Side8](https://previews.123rf.com/images/saiko3p/saiko3p1709/saiko3p170900026/85225245-hohes-schloss-f%C3%BCssen-oder-gotisches-hohes-schloss-der-bisch%C3%B6fe-und-kloster-klosterkirche-luftpanorama-in-f%C3%BCssen-d.jpg)
- [Side9](https://c8.alamy.com/compde/py6d75/fussen-hohes-schloss-hohes-schloss-und-kloster-st-mang-links-landkreis-ostallbrau-allgauer-bayern-deutschland-py6d75.jpg)
- [Side10](https://www.fahrrad-tour.de/ViaClaudiaAugusta/BilderEtappe_2/HohesSchloss_P7060434.jpg)
- [Side11](http://www.burgenwelt.org/deutschland/fuessen/bild5.jpg)
- [Side12](https://www.neugebauer-allgaeu.de/fileadmin/Bilder/Ausflugsziele/Hohes-Schloss-Fuessen.jpg)
- [Side13](https://ausfluege.info/wp-content/uploads/2019/11/hohes-schloss-fuessen-eingang-1024x768.jpg)

### Arial
- [Arial](https://i.redd.it/ft2ihbyhy1z31.jpg)
- [Arial1](https://www.robert-klinger.de/wp-content/uploads/2016/01/Fu%CC%88ssen.jpg)
- [Arial2](https://www.nuernbergluftbild.de/images/luftbild/K06250395.JPG)
- [Arial3](https://www.robert-klinger.de/wp-content/uploads/2016/01/Das-Hohe-Schloss-Fu%CC%88ssen.jpg)
- [Arial4](https://c8.alamy.com/compde/kwyj0w/hohes-schloss-fussen-oder-gotische-hohe-schloss-der-bischofe-antenne-panoramaaussicht-deutschland-hohes-schloss-liegt-auf-einem-hugel-uber-fussen-altstadt-in-swabi-kwyj0w.jpg)
- [Arial5](https://previews.123rf.com/images/saiko3p/saiko3p1810/saiko3p181000324/109052251-hohes-schloss-fussen-or-gothic-high-castle-of-the-bishops-aerial-panoramic-view-germany-hohes-schlos.jpg)
- [Arial6](https://prevs.allefotografen.de/e2225af4673f7e2aa9978fc703e5d7e9/medium_hohes-schloss-fuessen-2015-3938.jpg?v=1613386850)
- [Arial7](https://upload.wikimedia.org/wikipedia/de/f/f9/Hohes_Schloss_Fuessen_.jpg?1626877707505)
- [Arial8](https://3dwarehouse.sketchup.com/warehouse/v1.0/publiccontent/3ad7240b-bdd0-446c-b51c-325a7b4e184c)

### Courtyard
- [Courtyard](https://www.burgerbe.de/wp-content/uploads/2015/12/Hohes-Schloss-Fuessen-Gefaengnisturm.jpeg)
- [Courtyard1](https://thumbs.dreamstime.com/z/hohes-schloss-mittelalterliches-schloss-mitten-alter-stadt-fussen-91345959.jpg)
- [Courtyard2](https://www.burgerbe.de/wp-content/uploads/2015/12/Schloss-Fuessen-malereien.jpeg) - Used for Grey, Yellow, Red Painted Bricks on Tower, open grey arches
- [Courtyard3](https://schulbuch.ostallgaeu.de/fileadmin/_migrated/pics/Hohes_Schloss_1_klein.jpg)
- [Courtyard4](https://www.imago-images.de/bild/st/0096463214/w.jpg)
- [Courtyard5](https://c8.alamy.com/compde/rbj55a/trompe-loeil-gemalde-an-hohes-schloss-schloss-fussen-ostallgau-bayern-deutschland-rbj55a.jpg)
- [Courtyard6](https://weltenbummlerin.net/wp-content/gallery/fuessen_hohes_schloss/fuessen_31.jpg)
- [Courtyard7](http://photos.wikimapia.org/p/00/07/54/64/55_big.jpg)
- [Courtyard8](https://rfehrdoteu.files.wordpress.com/2019/08/2019_08_14-202340a-fc3bcssen-hohes-schloss-mit-illusionsmalerei-e1566638608338.jpg?w=583&h=318)
- [Courtyard9](https://www.allgaeueralpen.com/wp-content/uploads/allgau_fussen_stadtportraitdlehmann-9.jpg)
- [Courtyard10](https://djv-bildportal.de/s/image/Das-Hohe-Schloss-in-F-ssen-Allg-u-0005415850.jpg)

### Misc
- [GroundPlan](https://burgenarchiv.de/grafiken/grundrisse/bay/hohes-schloss-fuessen.jpg)

## Marksburg
- [Marksburg_Arial](https://www.marksburg.de/assets/uploads/Luftaufnahme_Nordseite.jpg)
