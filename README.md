# HoloLens 2 Castle Simulation
- Master thesis project, creating a castle simulation with Unreal Engine 4.26.2, HoloLens 2 and the MRTK for Unreal.
- Project naming conventions and style guide: [Gamemakin UE4 Style Guide by Allar](https://github.com/Allar/ue4-style-guide)

## Tools Used:
- [Blender 2.92.0](https://www.blender.org/)
    - [BlenderTools/ Send2Unreal](https://github.com/epicgames/blendertools) (link only works if you are member of the "Epic Games" organization on github!)
    - [Own ULTA Addon](https://github.com/JonasErkert/ULTA)
- [Node Graph Assistant](https://www.unrealengine.com/marketplace/en-US/product/node-graph-assistant)
- [Visual Studio 2019 Community 16.10.0](https://visualstudio.microsoft.com/de/vs/)
- [Rider for Unreal Engine 2021.1.2](https://www.jetbrains.com/de-de/lp/rider-unreal/)
- [PureRef](https://www.pureref.com/)
- Substance Designer 2020.1.2
- Affinity Photo
- Affinity Designer
- [FontForge](https://fontforge.org/en-US/)
- [Audacity 3.0.0](https://www.audacity.de/)

## Third Party Assets:
- [UX Tools](https://github.com/microsoft/MixedReality-UXTools-Unreal)
- [Graphics Tools](https://github.com/microsoft/MixedReality-GraphicsTools-Unreal)
- [Mixamo](https://www.mixamo.com/)
    - Animations:
        - Floating, Quick Formal Bow, Salute, Standing Greeting, Waving, Angry
    - Retargeting Tool:
        - https://aaronhunt.gumroad.com/l/yICUa
    - Characters:
        - Paladin J Nordstrom
- [Marksburg Image](https://commons.wikimedia.org/wiki/File:Marksburg-Braubach.jpg) by [Tobi 87](https://commons.wikimedia.org/wiki/User:Tobi_87)
- [PlaceBuilding Sound](https://freesound.org/people/millonlazaruspillay/sounds/366242/) by [millonlazaruspillay](https://freesound.org/people/millonlazaruspillay/)