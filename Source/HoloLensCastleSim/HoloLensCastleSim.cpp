// Copyright Epic Games, Inc. All Rights Reserved.

#include "HoloLensCastleSim.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, HoloLensCastleSim, "HoloLensCastleSim" );
