// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HoloLensCastleSimGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class HOLOLENSCASTLESIM_API AHoloLensCastleSimGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
